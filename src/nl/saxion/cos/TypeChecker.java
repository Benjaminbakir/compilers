package nl.saxion.cos;


public class TypeChecker extends TaalBaseVisitor<Type> {

    private void validateType(TaalParser.ExpressionContext ctx, Type type) {
        Type inferred = visit(ctx);
        if (inferred != type) {
            throw new CompilerException(ctx, "expected " + type.toString() + " but got " + inferred.toString());
        }
    }

    @Override
    public Type visitProgram(TaalParser.ProgramContext ctx) {
        return super.visitProgram(ctx);
    }

    @Override
    public Type visitVariableDeclaratie(TaalParser.VariableDeclaratieContext ctx) {
        return super.visitVariableDeclaratie(ctx);
    }

    @Override
    public Type visitIfStatement(TaalParser.IfStatementContext ctx)
    {

        return super.visitIfStatement(ctx);
    }

    @Override
    public Type visitWhileLoop(TaalParser.WhileLoopContext ctx) {
        validateType(ctx.left, Type.BOOLEAN);
        validateType(ctx.right, Type.BOOLEAN);
        validateType(ctx.left, Type.INT);
        validateType(ctx.right, Type.INT);
        return Type.BOOLEAN;
    }

    @Override
    public Type visitAssignmentStatement(TaalParser.AssignmentStatementContext ctx) {
        return super.visitAssignmentStatement(ctx);
    }

    @Override
    public Type visitPrintStatement(TaalParser.PrintStatementContext ctx) {
        return super.visitPrintStatement(ctx);
    }

    @Override
    public Type visitExpressionS(TaalParser.ExpressionSContext ctx) {
        return super.visitExpressionS(ctx);
    }

    @Override
    public Type visitExNegate(TaalParser.ExNegateContext ctx) {
        return super.visitExNegate(ctx);
    }



    @Override
    public Type visitExMulOp(TaalParser.ExMulOpContext ctx) {
        validateType(ctx.left, Type.INT);
        validateType(ctx.right, Type.INT);
        return Type.INT;
    }

    @Override
    public Type visitExAddOp(TaalParser.ExAddOpContext ctx) {
        validateType(ctx.left, Type.INT);
        validateType(ctx.right, Type.INT);
        return Type.INT;
    }

    @Override
    public Type visitExBoolLiteral(TaalParser.ExBoolLiteralContext ctx) {
        validateType(ctx.left, Type.INT);
        validateType(ctx.right, Type.INT);
        return Type.BOOLEAN;
    }

    @Override
    public Type visitExIntLiteral(TaalParser.ExIntLiteralContext ctx) {
        return Type.INT;
    }

    @Override
    public Type visitExParentheses(TaalParser.ExParenthesesContext ctx) {
        return super.visitExParentheses(ctx);
    }


    @Override
    public Type visitVergelijkingExpressie(TaalParser.VergelijkingExpressieContext ctx) {
        return super.visitVergelijkingExpressie(ctx);
    }
}
