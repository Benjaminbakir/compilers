package nl.saxion.cos;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;
import java.util.Arrays;

public class Test {

    private static void evaluate(String line){

        CharStream charStream = CharStreams.fromString(line);
        TaalLexer lexer = new TaalLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        TaalParser parser = new TaalParser(tokens);
        ParseTree statement = parser.statement();

        checkEvaluateAndPrint(statement);


    }


    private static void checkEvaluateAndPrint(ParseTree parseTree ){
        try {
            TypeChecker typeChecker = new TypeChecker();
            Type type = typeChecker.visit(parseTree);

            Evaluator evaluator = new Evaluator("hoi");
            ArrayList<String> value = evaluator.visit(parseTree);

            for(String values : value)   {
                System.out.println(values);
            }


        }catch (CompilerException e){
            System.err.println("ERROR: " + e.getMessage());
        }
    }


    public static void main(String[] args) {
        String line = "print 5 .";
        evaluate(line);



    }

}



