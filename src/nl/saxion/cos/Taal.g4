grammar Taal;

program     : (statement)*;
statement   : TYPE ID ( EQUALS  expression  )? POINT                            #VariableDeclaratie
            | IF '('vergelijking')' '{' statement* '}'                          #IfStatement
            | WHILE  '('(BOOLEAN | vergelijking | ID)? ')' '{' statement* '}'   #WhileLoop
            | ID (EQUALS| SUM)? expression POINT                                #AssignmentStatement
            | PRINT ( expression | ID )? POINT                                  #PrintStatement
            | expression                                                        #ExpressionS
            ;

expression: '(' expression ')'                                              # ExParentheses
          | '-' expression                                                  # ExNegate
          | left=expression op=('*' | '/' ) right=expression                # ExMulOp
          | left=expression op=('+' | '-') right=expression                 # ExAddOp
          | INT                                                             # ExIntLiteral
          | BOOLEAN                                                         # ExBoolLiteral
          ;

vergelijking :  left=expression op=('<' | '<=' | '>' | '>=' | EQUALSNOT | EQUALS) right=expression  #VergelijkingExpressie ;

INT: '0' | [1-9][0-9]*;
BOOLEAN: 'true' | 'false' ;
TYPE: 'int' |'boolean';
EQUALS: 'is' ;
EQUALSNOT : 'isnot';
POINT: '.';
IF : 'if';
SUM : 'telop';
WHILE : 'loop';
PRINT : 'print';
ID: [a-zA-Z_][a-zA-Z0-9_]*;
WS: [\r\n\t ]+ -> skip;

