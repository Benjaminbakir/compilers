package nl.saxion.cos;

import java.util.ArrayList;

public class Evaluator extends TaalBaseVisitor<ArrayList<String>> {

    private String name;

    private int sequence =0;

    private ArrayList<String> variables = new ArrayList<>();

    public Evaluator(String name) {
        this.name = name;
    }

    @Override
    public ArrayList<String> visitExAddOp(TaalParser.ExAddOpContext ctx) {
        ArrayList<String> assembly = new ArrayList<>();
        assembly.addAll(visit(ctx.left));
        assembly.addAll(visit(ctx.right));
        assembly.add(ctx.op.getText().equals("+") ? "\tiadd" : "\tisub");

        return assembly;
    }

    @Override
    public ArrayList<String> visitExIntLiteral(TaalParser.ExIntLiteralContext ctx) {
        ArrayList<String> assembley = new ArrayList<>();
        assembley.add( "\tldc " + ctx.INT().getText());

        return assembley;
    }

    @Override
    public ArrayList<String> visitProgram(TaalParser.ProgramContext ctx) {
        ArrayList<String> assembly = new ArrayList<>();
        assembly.add("\t.limit stack 10");
        assembly.add("\t.limit locals 56");
        for (TaalParser.StatementContext statementContext : ctx.statement()) {
            assembly.addAll(visit(statementContext));
        }
        return assembly;
    }

    @Override
    public ArrayList<String> visitExNegate(TaalParser.ExNegateContext ctx) {
        ArrayList<String> assembly = new ArrayList<>();
        assembly.add("\tldc" + ctx.getText());

        return assembly;    }


    @Override
    public ArrayList<String> visitExMulOp(TaalParser.ExMulOpContext ctx) {
        ArrayList<String> assembly = new ArrayList<>();
        assembly.addAll(visit(ctx.left));
        assembly.addAll(visit(ctx.right));
        assembly.add(ctx.op.getText().equals("*") ? "\timul" : "\tidiv");

        return assembly;    }

    @Override
    public ArrayList<String> visitExBoolLiteral(TaalParser.ExBoolLiteralContext ctx) {
        ArrayList<String> assembly = new ArrayList<>();

        return assembly;
    }

    @Override
    public ArrayList<String> visitExParentheses(TaalParser.ExParenthesesContext ctx) {
        ArrayList<String> assembley = new ArrayList<>();
        assembley.addAll(visit(ctx.expression()));
        return assembley;    }



    @Override
    public ArrayList<String> visitVariableDeclaratie(TaalParser.VariableDeclaratieContext ctx) {
        ArrayList<String> assembly = new ArrayList<>();
        String id = ctx.ID().getText();
        int offset = variables.indexOf(id) + 1; // Geeft de index/offset terug van de locatie waar het opgegeven 'id' staat.
        if (offset == 0) { // Bestaat nog niet
            variables.add(id);
            offset = variables.size();
        }
        assembly.addAll(visit(ctx.expression()));
        assembly.add("\tistore\t" + offset);
        return assembly;
    }

    @Override
    public ArrayList<String> visitIfStatement(TaalParser.IfStatementContext ctx) {
        return super.visitIfStatement(ctx);
    }

    @Override
    public ArrayList<String> visitWhileLoop(TaalParser.WhileLoopContext ctx) {
        ArrayList<String> assembly = new ArrayList<>();
        int uniek = ++sequence;
        assembly.addAll(visit(ctx.vergelijking()));
        assembly.add("EnterRepeat" + uniek+ ":");
        assembly.add("\tdup");
        assembly.add("\tifle\tExitRepeat"+ uniek);
        for(TaalParser.StatementContext statment : ctx.statement()){
            assembly.addAll(visit(statment));
        }
        assembly.add("\ticonst_1");
        assembly.add("\tisub");
        assembly.add("\tgoto\tEnterRepeat" + uniek);
        assembly.add("ExitRepeat" + uniek + ":");
        assembly.add("\tpop");

        return  assembly;
    }

    @Override
    public ArrayList<String> visitAssignmentStatement(TaalParser.AssignmentStatementContext ctx) {
        ArrayList<String> assembly = new ArrayList<>();
        String id = ctx.ID().getText();
        int offset = variables.indexOf(id) + 1; // Geeft de index/offset terug van de locatie waar het opgegeven 'id' staat.
        if (offset != 0) {
            assembly.addAll(visit(ctx.expression()));
            assembly.add("\tistore\t" + offset); }

            return assembly;

    }

    @Override
    public ArrayList<String> visitPrintStatement(TaalParser.PrintStatementContext ctx) {

        ArrayList<String> assembly = new ArrayList<>();
        assembly.add("\tgetstatic\tjava/lang/System.out\tLjava/io/PrintStream;");
        assembly.addAll(visit(ctx.expression()));
        assembly.add("\tinvokevirtual\tjava/io/PrintStream.println(I)V");
        assembly.add("java/io/PrintStream/print(Ljava/lang/String;)V");
        return assembly;
    }

    @Override
    public ArrayList<String> visitExpressionS(TaalParser.ExpressionSContext ctx) {
        return super.visitExpressionS(ctx);
    }

    @Override
    public ArrayList<String> visitVergelijkingExpressie(TaalParser.VergelijkingExpressieContext ctx) {
        return super.visitVergelijkingExpressie(ctx);
    }
}
