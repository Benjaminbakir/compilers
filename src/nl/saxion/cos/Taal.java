package nl.saxion.cos;


import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Taal {

    private String defaultCode  =
           "int a is 5.";

    private String startProg = ".class public {{name}}\n" +
            ".super java/lang/Object\n" +
            "\n" +
            ";\n" +
            "; standard initializer (calls java.lang.Object's initializer)\n" +
            ";\n" +
            ".method public <init>()V\n" +
            "\taload_0\n" +
            "\tinvokenonvirtual java/lang/Object/<init>()V\n" +
            "\treturn\n" +
            ".end method\n" +
            "\n" +
            ";\n" +
            "; main() method\n" +
            ";\n" +
            ".method public static main([Ljava/lang/String;)V\n";


    private String endProg = "\n\nreturn\n\n.end method";

    public void compiler(String[] args){
        String compileString = "";
        String name = "";
        if (args.length == 0) {
            compileString = defaultCode;
            name = "DefaultCode";
        } else {
            File f = new File(args[0]);
            if (!f.exists()) {
                System.out.println("File does not exist");
                return;
            }
            try {
                compileString = new String(Files.readAllBytes(Paths.get(args[0])));
            } catch (IOException ex) {
                ex.printStackTrace();
                return;
            }
            name = f.getName();
            if (name.lastIndexOf(".") > -1)
                name = name.substring(0, name.lastIndexOf("."));
        }
        CharStream charStream = CharStreams.fromString(compileString);
        TaalLexer lexer = new TaalLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        TaalParser parser = new TaalParser(tokens);
        ParseTree program = parser.program();
        Evaluator visitor = new Evaluator(name);
        ArrayList<String> prog = visitor.visit(program);

        System.out.println(startProg.replaceAll("\\{\\{name\\}\\}",name));
        // Output compiled part of the jasmin file
        System.out.println(prog.stream().collect(Collectors.joining("\n")));
        // Output footer of jasmin file
        System.out.println(endProg);

    }

    public static void main(String[] args) {
        Taal taal = new Taal();
        taal.compiler(args);
    }
}
